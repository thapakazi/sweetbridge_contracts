// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


pragma solidity ^0.4.23;

import "./LockedAssets.sol";
import "./Wallet.sol";
import "./tokens/Token.sol";
import "./Math.sol";
import "./authority/Roles.sol";


contract VaultFactoryI {
    function createVault(address wallet) public returns (address);
    function revokeMintingRightsFromVault() public;
}

contract VaultFactory is SecuredWithRoles {

    constructor(address rolesContract) public SecuredWithRoles("VaultFactory", rolesContract) {}

    function createVault(address wallet) public roleOrOwner("vaultCreator") returns (address) {
        require(WalletI(wallet).roles() == address(roles));
        Vault vault = new Vault(wallet, this);
        SecuredWithRolesI brg = SecuredWithRolesI(WalletI(wallet).brg());
        roles.grantUserRole(brg.contractHash(), "minter", vault);
        return address(vault);
    }

    // only a minter can call this function. all the vaults are minters
    // additionally the caller must be a contract with a wallet() function
    // as we must never grant the minter role to an untrusted party, this should be verification enough
    function revokeMintingRightsFromVault() public {
        VaultI sendingVault = VaultI(msg.sender);
        WalletI sendingWallet = WalletI(sendingVault.wallet());
        require(SecuredWithRolesI(sendingWallet.brg()).userHasRole("minter", msg.sender));
        roles.revokeUserRole(SecuredWithRolesI(sendingWallet.brg()).contractHash(), "minter", msg.sender);
        sendingWallet.removeVault(msg.sender);
    }
}

contract VaultEvents {
    event UouRequested(uint256 brgAmount);
    event UouRequestApproved(uint256 brgAmount, uint256 feeAmount);
    event UouRequestDeclined(uint256 brgAmount);
    event BridgecoinsTransferredToTreasury(address treasury, uint256 wad);
}

contract VaultI {
    function wallet() public returns (address);
}

/*Each wallet belongs to exactly one user who is the owner of the contract
  the tokens sent to the wallet belong to it and can only be transfered by calling the transfer
  function on the wallet */
contract Vault is LockedAssets, VaultEvents {
    using Math for uint256;

    uint256 public requestedAmount;
    uint256 public fee; // the fee which must be included in the repaid amount
    WalletI public wallet;
    uint256 amountDue;
    address factory;

    constructor(address wallet_, address factory_)
        LockedAssets([address(WalletI(wallet_).swc()), address(WalletI(wallet_).brg())],
        "Vault", WalletI(wallet_).roles()) public
    {
        amountDue = 0;
        requestedAmount = 0;
        fee = 0;
        wallet = WalletI(wallet_);
        owner = wallet.owner();
        factory = factory_;
    }

    function treasuryAddress() view public returns (address) {
        return wallet.treasuryAddress();
    }

    // Handle repayment of the vault UOU by BRC transfer.
    function tokenFallback(address sender, uint value, bytes) public {
        /*if the token which was sent is Bridgecoin*/
        if (msg.sender == address(wallet.brg())) {
            // Send all the Bridgecoin to the treasury and let it decide by how much to reduce the amount due
            if (TokenI(wallet.brg()).transfer(treasuryAddress(), value)) {
                emit BridgecoinsTransferredToTreasury(treasuryAddress(), value);
            }
        }
    }

    function balance() public view returns (uint256) {
        return amountDue.add(fee);
    }

    function destroy() public onlyOwner {
        require(amountDue == 0);
        for (uint32 i = 0; i < assets.length; ++i) {
            assets[i].transfer(wallet, assets[i].balanceOf(this));
        }
        VaultFactoryI(factory).revokeMintingRightsFromVault();
        selfdestruct(wallet);
    }

    function setBalance(uint256 amountDue_, uint256 fee_) public onlyRole("treasuryOracle") {
        amountDue = amountDue_;
        fee = fee_;
    }

    function requestUou(uint256 brgAmount) public onlyOwner {
        require(requestedAmount == 0);
        require(hasFunds());
        requestedAmount = brgAmount;
        emit UouRequested(brgAmount);
    }

    function acceptUouRequest(uint256 fees_) public onlyRole("uouOracle") {
        require(requestedAmount > 0);
        fee = fee.add(fees_);
        amountDue = amountDue.add(requestedAmount);
        TokenI(wallet.brg()).mintFor(address(wallet), requestedAmount);
        emit UouRequestApproved(requestedAmount, fee);
        requestedAmount = 0;
    }

    function rejectUouRequest() public onlyRole("uouOracle") {
        require(requestedAmount > 0);
        emit UouRequestDeclined(requestedAmount);
        requestedAmount = 0;
    }
}
