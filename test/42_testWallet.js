// Copyright (c) 2017 Sweetbridge Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var VaultFactory = artifacts.require('VaultFactory')
var Roles = artifacts.require('Roles')
var Wallet = artifacts.require('Wallet')
var Vault = artifacts.require('Vault')
var SWT = artifacts.require('SweetToken')
var BRG = artifacts.require('BridgeToken')
let utils = require('./utils')

contract('Wallet', function (accounts) {
  const RequestType = {initial: 0, transfer: 1, removal: 2}
  const RequestState = {initial: 0, pending: 1, executed: 2, rejected: 3}
  const user = accounts[5]
  let wallet
  let transferOracle = accounts[7]
  let treasuryOracle = accounts[8]
  let vaultFactory
  let vaultAddrs = []
  let swt
  let brg
  let roles
  const fs = require('fs')
  const fscb = function () { /* empty callback */ }

  before(async () => {
    swt = await SWT.deployed()
    brg = await BRG.deployed()
    const tx = await swt.transfer(user, 1.5e21, {from: accounts[15]})
    fs.appendFile('gas.csv', 'SweetToken;transfer;' + tx.receipt.gasUsed + '\n', fscb)
    vaultFactory = await VaultFactory.deployed()
    roles = await Roles.deployed()
  })

  it('can be created', async () => {
    wallet = await Wallet.new(user, Roles.address, "USD", SWT.address, BRG.address, VaultFactory.address)
    //TODO add test to verify that without this role the wallet cannot create vaults
    await roles.grantUserRole(await vaultFactory.contractHash(), "vaultCreator", wallet.address)
    assert.equal(await wallet.owner(), user)
  })

  it('assigns roles to participants', async () => {
    await utils.addRole('transferOracle', wallet, transferOracle)
    assert.isOk(await wallet.userHasRole('transferOracle', transferOracle))
  })

  it('removes the default SWT and BRG asset from the wallet', async () => {
    const len = await wallet.assetsLen()
    const asset0 = await wallet.assets(0)
    const txRmAsset = await wallet.rmAsset(SWT.address, accounts[1], {from: user})
    fs.appendFile('gas.csv', 'Wallet;rmAsset;' + txRmAsset.receipt.gasUsed + '\n', fscb)
    const removed = txRmAsset.logs.find(log => log.event === 'AssetRemoved').args
    const txRmAsset2 = await wallet.rmAsset(BRG.address, accounts[1], {from: user})
    fs.appendFile('gas.csv', 'Wallet;rmAsset;' + txRmAsset2.receipt.gasUsed + '\n', fscb)
    const removed2 = txRmAsset2.logs.find(log => log.event === 'AssetRemoved').args

    assert.equal(len.toNumber(), 2)
    assert.equal(asset0, SWT.address)
    assert.isOk(txRmAsset)
    assert.equal(removed.token, SWT.address)
    assert.isOk(txRmAsset2)
    assert.equal(removed2.token, BRG.address)
  })


  it('can create vaults', async () => {
    let txVault1 = await wallet.addVault({from: user})
    fs.appendFile('gas.csv', 'Wallet;addVault;' + txVault1.receipt.gasUsed + '\n', fscb)
    let newVault1 = txVault1.logs.find(log => log.event === 'LogVaultAdded').args
    vaultAddrs.push(newVault1.vault)
    let txVault2 = await wallet.addVault({from: user})
    fs.appendFile('gas.csv', 'Wallet;addVault;' + txVault2.receipt.gasUsed + '\n', fscb)
    newVault2 = txVault2.logs.find(log => log.event === 'LogVaultAdded').args
    vaultAddrs.push(newVault2.vault)

    assert.equal((await wallet.vaultCount()).toNumber(), 2, 'there should be 2 vaults')
    assert.ok((await brg.userHasRole("minter", vaultAddrs[0])))
    assert.ok((await brg.senderHasRole("minter", {from: vaultAddrs[0]})))
    assert.equal(brg.address, await wallet.brg(), 'wallet should have same contract for brg')
  })

  it('can remove a vault', async () => {
    const vaultToDestroy = vaultAddrs[0]
    const txDestroy = (await Vault.at(vaultToDestroy).destroy({from: user}))
    fs.appendFile('gas.csv', 'Vault;remove;' + txDestroy.receipt.gasUsed + '\n', fscb)
    const remainingVaults = await wallet.vaultCount()
    assert.equal(remainingVaults.toNumber(), 1, 'there must be 1 vault left')
  })

  it('can add SWC as an asset', async () => {
    await swt.transfer(user, 2e21, {from: accounts[12]})
    const swtBalance = await swt.balanceOf(user)

    const tx = await swt.approve(wallet.address, 1e21, {from: user, gas: 1000000})
    await fs.appendFile('gas.csv', 'SweetToken;approve;' + tx.receipt.gasUsed + '\n', fscb)
    const vaultTx = await wallet.addAsset(swt.address, user, 6e20, {from: user})
    await fs.appendFile('gas.csv', 'Vault;addAsset;' + vaultTx.receipt.gasUsed + '\n', fscb)

    const len = (await wallet.assetsLen()).toNumber()
    const asset0 = await wallet.assets(0)

    const balance = (await wallet.balanceOf(SWT.address)).toNumber()
    const vaultAddr = (await swt.balanceOf(wallet.address)).toNumber()
    const usrBalance = (await swt.balanceOf(user)).toNumber()

    assert.equal(len, 1)  // check assets len
    assert.equal(asset0, SWT.address)
    assert.equal(balance, 6e20)
    assert.equal(vaultAddr, 6e20)
    assert.equal(usrBalance, swtBalance.minus(6e20))  // 1e22 = initial SWT supply
  })

  it('refers a token transfer request to the oracle', async () => {
    let startBalance = await swt.balanceOf(user)
    let vaultStartBalance = await swt.balanceOf(wallet.address)
    let tx = await wallet.transfer(swt.address, user, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await swt.balanceOf(wallet.address)).toNumber())

    tx = await wallet.resolveRequest(RequestType.transfer, RequestState.executed, {from: transferOracle})
    let requestApproval = tx.logs.find(log => log.event === 'AssetTransferRequestApproved')
    assert.ok(requestApproval)

    assert.equal(startBalance.add(1e18).toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.sub(1e18).toNumber(), (await swt.balanceOf(wallet.address)).toNumber())
    await swt.transfer(wallet.address, 1e18, {from: user})
    assert.equal((await wallet.balanceOf(SWT.address)).toNumber(), 6e20)

  })

  it('keeps the same balance when the transfer request is rejected', async () => {
    let startBalance = await swt.balanceOf(user)
    let vaultStartBalance = await swt.balanceOf(wallet.address)
    let tx = await wallet.transfer(swt.address, user, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await swt.balanceOf(wallet.address)).toNumber())

    tx = await wallet.resolveRequest(RequestType.transfer, RequestState.rejected, {from: transferOracle})
    let requestRejection = tx.logs.find(log => log.event === 'AssetTransferRequestRejected')
    assert.ok(requestRejection)

    assert.equal(startBalance.toNumber(), (await swt.balanceOf(user)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await swt.balanceOf(wallet.address)).toNumber())
  })

  it('can add ETH as an asset', async () => {
    const ethBalance = await web3.eth.getBalance(user)
    assert.isAtLeast(ethBalance.toNumber(), 2e18, 'user must have 2e18 to allow access to them')
    let tx = await web3.eth.sendTransaction({to: wallet.address, value: 2e18, from: user})

    const balance = (await web3.eth.getBalance(wallet.address)).toNumber()

    assert.equal(balance, 2e18)
  })

  it('refers an ETH transfer request to the oracle', async () => {

    const startBalance = await web3.eth.getBalance(treasuryOracle)
    let vaultStartBalance = await web3.eth.getBalance(wallet.address)
    let tx = await wallet.transferEth(treasuryOracle, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await web3.eth.getBalance(treasuryOracle)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await web3.eth.getBalance(wallet.address)).toNumber())

    tx = await wallet.resolveRequest(RequestType.transfer, RequestState.executed, {from: transferOracle})
    let requestApproval = tx.logs.find(log => log.event === 'AssetTransferRequestApproved')
    assert.ok(requestApproval)

    assert.equal((await web3.eth.getBalance(wallet.address)).toString(10), vaultStartBalance.sub(1e18).toString(10))
    assert.equal((await web3.eth.getBalance(treasuryOracle)).toString(10), startBalance.add(1e18).toString(10))
  })

  it('keeps the ETH balance unchanged if the transfer request is rejected', async () => {

    const startBalance = await web3.eth.getBalance(treasuryOracle)
    let vaultStartBalance = await web3.eth.getBalance(wallet.address)
    let tx = await wallet.transferEth(treasuryOracle, 1e18, {from: user})
    let assetTransferRequest = tx.logs.find(log => log.event === 'AssetTransferRequest')

    assert.isOk(assetTransferRequest)
    assert.equal(startBalance.toNumber(), (await web3.eth.getBalance(treasuryOracle)).toNumber())
    assert.equal(vaultStartBalance.toNumber(), (await web3.eth.getBalance(wallet.address)).toNumber())

    tx = await wallet.resolveRequest(RequestType.transfer, RequestState.rejected, {from: transferOracle})
    let requestRejection = tx.logs.find(log => log.event === 'AssetTransferRequestRejected')
    assert.ok(requestRejection)

    assert.equal((await web3.eth.getBalance(wallet.address)).toString(10), vaultStartBalance.toString(10))
    assert.equal((await web3.eth.getBalance(treasuryOracle)).toString(10), startBalance.toString(10))
  })

})
